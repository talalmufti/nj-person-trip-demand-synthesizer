'''
New Jersey Person Trip Synthesizer

Module 3:   Assign schools, colleges, and universities to appropriate NJ
            residents.
            '''

__author__      = "Talal Mufti <talalmufti@gmail.com>"
__version__     = "0.10a"
__date__        = "7/23/2012"
__license__     = "python"

import csv
import random as rd
import numpy as np
from numpy import *
import math as m
from datetime import datetime

M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"
school_path = 'Data\\School\\'

IDs = ['ATL', 	'BER', 	'BUR', 	'CAM', 	'CAP', 	'CUM', 	'ESS',\
      'GLO', 	'HUD', 	'HUN', 	'MER', 	'MID', 	'MON', 	'MOR',\
      'OCE', 	'PAS', 	'SAL', 	'SOM', 	'SUS', 	'UNI', 	'WAR',\
      'NYC',    'PHL',  'BUC',  'SOU',  'NOR',  'WES',  'ROC']

special_perc = 0.00618
enrollment_ages = { 0: (3,4),
                    1: (5,6),
                    2: (7,9),
                    3: (10,13),
                    4: (14,15),
                    5: (16,17),
                    6: (18,19),
                    7: (20,21),
                    8: (22,24) }

private_percent_ages = { 0:(3,4),
                         1:(5,9),
                         2:(10,14),
                         3:(15,17),
                         4:(18,19),
                         5:(20,24) }

EARTH_RAD  = 3963.17
FIPS = range(1,42,2)
def get_dict_key(a, d):
    'Returns the key to the range that age a falls into in dictionary d'
    for k in d:
        if a>=d[k][0] and a<=d[k][1]:
            return k

def expand_distribution(dist):
    'returns list that repeats each element index of dist "freq" times where "freq" is the value of that element'
    vec = [[i]*int(round(x)) for i, x in enumerate(dist)]
    return [num for elem in vec for num in elem]

def normalize_by_average(l, nzl):
    'l must be numpy ndarray. nzl is #non-zero elements in l. returns normalized vector'
    avg = sum(l)/nzl
    l = (l/avg+.5) #Prof K suggested l/avg +.5 before truncation, but I prefer this
    return trunc(l)

def select_one(l):
    'way to randomly select a single item from list. especially convenient for binary dist (True/False, fam/non-fam, under/over 18)'
    '''note that when an index has freq zero, it is not represented at all, which causes a problem whern there are only two indeces, one with freq 0'''
    r = np.random.randint(0,len(l)) if len(l)>1 else 0 # added else for when len(l) = 0
    #print 'r: ' + str(r)
    return l[r]

def get_school_type(age, wC, hht):
    #returns
    #0 - public es
    #1 - public ms
    #2 - public hs
    #3 - private es
    #4 - private ms
    #5 - private hs
    #6 - special needs
    #7 - Commuter (public) University
    #8 - non-Commuter (private) University
    #9 - not enrolled

    if hht in [2,3,4,5,7,8] or age<5 or age>24 or wC in xrange(42,50): #correctional, juvee, nursing, other inst/non-inst
        return 9
    elif hht == 6:
        return 8
    elif hht in [0,1]:
        if rd.random() < special_perc:#special
            return 6
        i = get_dict_key(age, enrollment_ages)
        enr_r = schoolEnroll[i]*10
        enr = select_one(expand_distribution(enr_r))
        if enr in [0,4]: #not enrolled or in nursery and not counted in model
            return 9
        j = get_dict_key(age, private_percent_ages)
        if enr == 3: #grad/undergrad
            if rd.random() >pr_p[j]:
                return 8
            elif rd.random() < 0.2255: #accounts for Rutgers which is pub but NonComm
                return 8
            else:
                return 7
        if enr == 2:
            if rd.random()<pr_p[j]:
                return 2
            else:
                return 5
        elif enr == 1:
            if rd.random()<pr_p[j]:
                if age>10:
                    return 1
                else:
                    return 0
            else:
                if age>10:
                    return 4
                else:
                    return 3

def distance(LatS, LonS, LatH, LonH):
    dLat = (LatS-LatH)*m.pi/180
    dLon = (LonS-LonH)*m.pi/180
    a = m.sin(dLat/2) * m.sin(dLat/2) + m.cos(LatH*pi/180) * m.cos(LatS*pi/180) * m.sin(dLon/2) * m.sin(dLon/2);
    c = 2 * math.asin(sqrt(a))
    return EARTH_RAD*c

def get_school(c, st, lat, lon):
    if st == 0:
        l = np.array([distance(lat, lon,latlon[0], latlon[1])
                      for latlon in pubeslatlon[c] ])
        i = l.argmin()
        return [FIPS[c]] + [pubes[c][i][j] for j in [0,2,5,6]]
    elif st == 1:
        l = np.array([distance(lat, lon,latlon[0], latlon[1])
                      for latlon in pubmslatlon[c] ])
        i = l.argmin()
        return [FIPS[c]] + [pubms[c][i][j] for j in [0,2,5,6]]
    elif st == 2:
        l = np.array([distance(lat, lon,latlon[0], latlon[1])
                      for latlon in pubhslatlon[c] ])
        i = l.argmin()
        return [FIPS[c]] + [pubhs[c][i][j] for j in [0,2,5,6]]
    elif st == 6:
        l = np.array([distance(lat, lon,latlon[0], latlon[1])
                      for latlon in pubsplatlon ])
        i = l.argmin()
        return [pubsp[i][j] for j in [1,0,2,5,6]]
    elif st == 3: #private es
        l = np.array([(distance(lat, lon,latlon[0], latlon[1]))**2
                      for latlon in pvteslatlon[c] ]) #if relaxing county constraint
        e = np.array(pvtesEnr[c])                       # simply retool latlon and ENR
        p = e/l                              # arrays to ignore county
        pe = expand_distribution(normalize_by_average(p, len(p)))                     #also add school County
        i = select_one(pe)
        return [FIPS[c]] + [pvtes[c][i][j] for j in [0,2,11,12]]
    elif st == 4:
        l = np.array([(distance(lat, lon,latlon[0], latlon[1]))**2
                      for latlon in pvtmslatlon[c] ])
        e = np.array(pvtmsEnr[c])
        p = e/l
        pe = expand_distribution(normalize_by_average(p, len(p)))
        i = select_one(pe)
        return [FIPS[c]] + [pvtms[c][i][j] for j in [0,2,11,12]]
    elif st == 5:
        l = np.array([(distance(lat, lon,latlon[0], latlon[1]))**2
                      for latlon in pvthslatlon[c] ])
        e = np.array(pvthsEnr[c])
        p = e/l
        pe = expand_distribution(normalize_by_average(p, len(p)))
        i = select_one(pe)
        return [FIPS[c]] + [pvths[c][i][j] for j in [0,2,11,12]]
    elif st == 7:
        l = np.array([(distance(lat, lon,latlon[0], latlon[1]))**2
                      for latlon in ComUnilatlon ])
        e = np.array(ComUniENR)
        p = e/l
        pe = expand_distribution(normalize_by_average(p, len(p)))
        i = select_one(pe)
        return [ComUni[i][j] for j in [1,0,2,5,6]] #needs to convert county code to FIPS
    elif st == 8:
        l = np.array([(distance(lat, lon,latlon[0], latlon[1]))**2
                      for latlon in NonComUnilatlon ])
        e = np.array(NonComUniENR)
        p = e/l
        pe = expand_distribution(normalize_by_average(p, len(p)))
        i = select_one(pe)
        return [NonComUni[i][j] for j in [1,0,2,5,6]]
    #no accounting for group quarters yet, but might not be necessary
    elif st == 9:
        return [-1,-1,-1,-1,-1]




##################################################


#Read in Private Schools, w/ overlap
pvtes = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]] #if flattening, make 1d
pvtms = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
pvths = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]

h = open(M_PATH+school_path+'Private schools.csv', 'rb')
pvtSchoolReader = csv.reader(h, delimiter=',')
header = pvtSchoolReader.next()
for rowNum, row in enumerate(pvtSchoolReader): #rowNum will act as a pointer into file
    c = (int(row[7])-1)/2 #county from 0-20
    ##print c
    ##print row[12]
    ##print row[13]
    if int(row[12])<12 and int(row[13])>=6: # must include at least up to 1st grade
        pvtes[c].append([rowNum]+row) #if flattening, remove [c]
    if int(row[12])<=12 and int(row[13])>=13: # must include at least grades 7 and 8
        pvtms[c].append([rowNum]+row)
    if int(row[12])<=14 and int(row[13])==17: # must include at least grades 9 to 12
        pvths[c].append([rowNum]+row)
h.close()

#these enrollment and lat/lon arrays are divided by county
pvtesEnr = [[int(row[16]) for row in pvtesC] for pvtesC in pvtes]
pvtmsEnr = [[int(row[16]) for row in pvtmsC] for pvtmsC in pvtms]
pvthsEnr = [[int(row[16]) for row in pvthsC] for pvthsC in pvths]

pvteslatlon = [[(float(row[11]), float(row[12])) for row in pvtesC] for pvtesC in pvtes]
pvtmslatlon = [[(float(row[11]), float(row[12])) for row in pvtmsC] for pvtmsC in pvtms]
pvthslatlon = [[(float(row[11]), float(row[12])) for row in pvthsC] for pvthsC in pvths]



#Read in Public Schools, Special Schools, Commuter and NonCommuter Universities
pubes = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
pubms = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
pubhs = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
pubsp = []
ComUni= []
NonComUni = [] # Drawing from NonComUni will not be constrained by county

les = open(M_PATH+school_path+'Elem.csv', 'rb')
lms = open(M_PATH+school_path+'Mid.csv', 'rb')
lhs = open(M_PATH+school_path+'High.csv', 'rb')
lsp = open(M_PATH+school_path+'Special.csv', 'rb')
lcu = open(M_PATH+school_path+'CommUniv.csv', 'rb')
ncu = open(M_PATH+school_path+'NonCommUniv.csv', 'rb')

listOfLists = [pubes, pubms, pubhs, pubsp, ComUni, NonComUni]
PubFileList = [les,lms,lhs,lsp,lcu,ncu]
for sNum, schoolFile in enumerate(PubFileList):
    pubSchoolReader = csv.reader(schoolFile, delimiter=',')
    header = pubSchoolReader.next()
    for rowNum, row in enumerate(pubSchoolReader):
        if sNum in [3,4,5]: # Drawing from Special/CommUni/NonComUni will not be constrained by county
            listOfLists[sNum].append([rowNum] + row[1:])
        else:
            c = int(row[1])
            listOfLists[sNum][c].append([rowNum] + row[1:])
    schoolFile.close()

pubeslatlon = [[(float(row[5]), float(row[6])) for row in pubesC] for pubesC in pubes]
pubmslatlon = [[(float(row[5]), float(row[6])) for row in pubmsC] for pubmsC in pubms]
pubhslatlon = [[(float(row[5]), float(row[6])) for row in pubhsC] for pubhsC in pubhs]
pubsplatlon = [(float(row[5]), float(row[6])) for row in pubsp]
ComUnilatlon =[(float(row[5]), float(row[6])) for row in ComUni]
NonComUnilatlon = [(float(row[5]), float(row[6])) for row in NonComUni]
ComUniENR = [float(row[4]) for row in ComUni]
NonComUniENR = [float(row[4]) for row in NonComUni]
##################################################
#Read in schoolenrollment file and private/public data
schoolEnroll = np.loadtxt(M_PATH+school_path+'schoolenrollment.csv',
                          delimiter = ',', skiprows=1)
private_percents = np.loadtxt(M_PATH+school_path+'ACS_10_3YR_S1401_with_ann.csv',
                  delimiter=',', skiprows=8, usecols=range(58,89,6))

###################### RUN ########################
path2 = M_PATH + 'Output\\Module 2\\'
path3 = M_PATH + 'Output\\Module 3\\'
startTime = datetime.now()
for countyNum, ID in enumerate(IDs):
    print (ID+ " started at " + str(datetime.now())
      + " duration: " + str(datetime.now()-startTime))
    f = open(path2 + str(ID + 'Module2NN.csv'), 'rb')
    personReader = csv.reader(f , delimiter= ',')
    g = open(path3 + str(ID + 'Module3NN.csv'), 'wb')
    personWriter = csv.writer(g , delimiter= ',')
    if countyNum>20:
        schoolType=9
        sch = [-1,-1,-1,-1,-1]
        for row in personReader:
            personWriter.writerow(row + [schoolType] + sch)
    else:
        pr_p = private_percents[countyNum]
        co = 0
        for row in personReader:
            schoolType = get_school_type(int(row[6]),int(row[11]),int(row[2]))
            sch = get_school(countyNum, schoolType, float(row[3]), float(row[4]))
            if co%10000==0:
                print co
            personWriter.writerow(row + [schoolType] + sch)
            co+=1
    f.close()
    g.close()








