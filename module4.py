'''
New Jersey Trip File Generator

Module 4: Assigns one of 18 Tour Types, or Activity Patterns, to each resident.
		  '''
__author__      = "Talal Mufti <talalmufti@gmail.com>"
__version__     = "0.40a"
__date__        = "12/31/2012"
__license__     = "python"

import csv
import random as rd
import numpy as np
from numpy import *
import math as m
from datetime import datetime

M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"
tour_path = 'Data\\Tours\\'

IDs = ['ATL', 	'BER', 	'BUR', 	'CAM', 	'CAP', 	'CUM', 	'ESS',\
      'GLO', 	'HUD', 	'HUN', 	'MER', 	'MID', 	'MON', 	'MOR',\
      'OCE', 	'PAS', 	'SAL', 	'SOM', 	'SUS', 	'UNI', 	'WAR',\
      'NYC',    'PHL',  'BUC',  'SOU',  'NOR',  'WES',  'ROC']

def expand_distribution(dist):
    'returns list that repeats each element index of dist "freq" times where "freq" is the value of that element'
    vec = [[i]*int(round(x)) for i, x in enumerate(dist)]
    return [num for elem in vec for num in elem]

def select_one(l):
    'way to randomly select a single item from list. especially convenient for binary dist (True/False, fam/non-fam, under/over 18)'
    '''creates a list that repeats each element	index of dist freq times where freq is the value of that element the chooses one uniformly
    note that when an index has freq zero, it is not represented at all, which causes a problem whern there are only two indeces, one with freq 0'''
    r = np.random.randint(0,len(l)-1) if len(l)>1 else 0 # added else for when len(l) = 0
    return l[r]

def revise_traveler_type(tt, st):
    'slight modification to traveler type from module 1'
    if tt in [1,2,3,4] and st == 9:
        return 6
    else:
        return tt



#read in TourType Distirbutions

#draw one for every person in every county

####### Run ########
tourTypeDistrib = np.loadtxt(M_PATH+tour_path+'TourTypes.csv', delimiter =',',
                             skiprows=0)
tourTypeDistrib*=1000 #exact distribution instead of approximate
tourTypeDistrib = np.transpose(tourTypeDistrib)
tourTypeDistribExp = []
for r in tourTypeDistrib:
    tourTypeDistribExp.append(expand_distribution(r))



path3 = M_PATH + 'Output\\Module 3\\'
path4 = M_PATH + 'Output\\Module 4\\'


startTime = datetime.now()
for countyNum, ID in enumerate(IDs):
    print(ID + " started at " + str(datetime.now())
      + " duration: " + str(datetime.now()-startTime))
    f = open(path3 + str(ID + 'Module3NN.csv'), 'rb')
    g = open(path4 + str(ID + 'Module4NN.csv'), 'wb')
    personReader = csv.reader(f , delimiter= ',')
    personWriter = csv.writer(g , delimiter= ',')
    co = 0
    for row in personReader:
        revisedTT = revise_traveler_type(int(row[8]), int(row[19]))
        TourType = select_one(tourTypeDistribExp[revisedTT])
        personWriter.writerow(row + [revisedTT] + [TourType])
        if co%10000==0:
            print co
        co+=1
    f.close()
    g.close()