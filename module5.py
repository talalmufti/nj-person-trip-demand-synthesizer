'''
New Jersey Person Trip Synthesizer

Module 1: Assign locations for patronage (Os) and build spatial trip chain
		  '''
__author__      = "Talal Mufti <talalmufti@gmail.com>"
__version__     = "0.40a"
__date__        = "12/31/2012"
__license__     = "python"


import csv
import random as rd
import numpy as np
from numpy import *
import math as m
from datetime import datetime
countyDic = dict(zip(range(1,42,2)+range(42,49), range(0,21)+range(21,28)))
EARTH_RAD  = 3963.17

def great_circle_distance(LatS, LonS, LatH, LonH):
    'This great circle distance function is functional but probably slightly slower than necessary'
    dLat = (LatS-LatH)*m.pi/180
    dLon = (LonS-LonH)*m.pi/180
    a = (m.sin(dLat/2) * m.sin(dLat/2) + m.cos(LatH*pi/180) * #replace first two terms with first term squared,
         m.cos(LatS*pi/180) * m.sin(dLon/2) * m.sin(dLon/2)) # repeat for next 2 terms
    c = 2 * math.asin(sqrt(a))
    return EARTH_RAD*c

def distance(LatS, LonS, LatH, LonH):
    'L2 distance function'
    return m.sqrt((LatH-LatS)**2 + (LonH-LonS)**2)

def normalize_by_average(l, nzl):
    'l must be numpy ndarray. nzl is #non-zero elements in l. returns normalized vector'
    avg = sum(l)/nzl
    l = (l/avg+.5)
    return trunc(l)

def normalize_by_average_two(l, nzl):
    'l must be numpy ndarray. nzl is #non-zero elements in l. returns normalized vector'
    avg = sum(l)/nzl
    l = (l*10/avg +.5) #Prof K suggested l/avg +.5 before truncation, but I prefer this
    return trunc(l)

def expand_distribution(dist):
    'returns list that repeats each element index of dist "freq" times where "freq" is the value of that element'
    vec = [[i]*int(round(x)) for i, x in enumerate(dist)] #should remove round
    return [num for elem in vec for num in elem]          # when using normalize

def select_one(l):
    'way to randomly select a single item from list. especially convenient for binary dist (True/False, fam/non-fam, under/over 18)'
    '''note that when an index has freq zero, it is not represented at all, which causes a problem whern there are only two indeces, one with freq 0'''
    r = np.random.randint(0,len(l)) if len(l)>1 else 0 # added else for when len(l) = 0
    return l[r]

def populate_neighboring_businesses(c):
    selfAndNeighbors = []
    for v in neighborMat[c]:
        n = countyDic[int(v)]
        selfAndNeighbors.extend(listOfLists[n])
    return selfAndNeighbors

def get_other(busList, latA, lonA, lunch = False):
    'return a list of length two containing lat and long of an other trip end'
    #lunch = True constrains selection to 5 mi radius
    if lunch:
        l = []
        lIndex=[]
        alt=1 #stub value which will updated as closest business to workplace
        nonZeroCount=0
        for arownum, arow in enumerate(busList):
            x,y = float(arow[3]),float(arow[4])
            dist = distance(x,y,latA, lonA)

            ##Here we check for businesses within .5 and 5 miles away from work
            #if dist>=.5 and dist<=5 and arow[2]!=0: #switch to this if using GCD
            if dist>=0.00803654465 and dist<=0.07381226849 and arow[2]!=0:
                l.append(arow[2]/dist**2)
                lIndex.append(arownum)
                nonZeroCount+=1
            elif dist<alt:
                alt=dist ##update closest business to work
                altIndex = arownum  ##update index for business above
        if len(l) ==0: ##if no businesses found within range
            s = altIndex ##choose closest
        else:
            l = normalize_by_average(np.array(l),nonZeroCount)
            s = lIndex[select_one(expand_distribution(l))]
    else:
        global block
        global RandomDrawVector
        global RDVIndex
        if latA!=block[0] and lonA!=block[1] or len(RandomDrawVector)==0:
            RandomDrawVector = []
            RDVIndex = []
            block[0] = latA
            block[1] = lonA
            nonZeroCount = 0
            for brownum, brow in enumerate(busList):
                x,y = float(brow[3]),float(brow[4])
                dist = distance(x,y,latA, lonA)
                if dist>=0.00803654465 and brow[2]!=0:
                    RandomDrawVector.append(brow[2]/dist**2)
                    RDVIndex.append(brownum)
                    nonZeroCount+=1
            RandomDrawVector = normalize_by_average(np.array(RandomDrawVector),nonZeroCount)
            RandomDrawVector = expand_distribution(RandomDrawVector)
        s = RDVIndex[select_one(RandomDrawVector)]
    return ['O'] + busList[s][0:2] + busList[s][3:6]

def create_tour(tourType, travelTy, workCountiesList, neighborSelfList, r):
    #Trip type, relevant county, pointer, lat, lon
    tour = []
    tourRepr = tourMat[tourType]
    wC = int(r[11])
    rC = int(r[0])
    for c in tourRepr:
        if c == "H":
            tour.extend(['H',r[0],-1]+r[3:5]+[-1]) #-1 saying there is no pointer
        elif c == "W":
            if travelTy==6:
                tour.extend(get_other(neighborSelfList, float(r[3]), float(r[4])))
            elif wC!=-1: #since some Traveler Type 3 are given tour types containing work
                tour.extend(['W', wC,r[18]]+r[16:18] + [r[12]])
        elif c == "S":
            tour.extend(['S', r[20], r[21]]+r[23:25]+[r[19]]) #currently school county is res county
        elif c == "O" and tourType==11 and wC!=-1 and wC not in xrange(42,49) or rC in xrange(42,49):
            tour.extend(get_other(workCountiesList[countyDic[wC]],
            float(r[16]),float(r[17]), True))
        else:
            tour.extend(get_other(neighborSelfList, float(r[3]), float(r[4])))
    return tour

##################################################################3

IDs = ['ATL', 	'BER', 	'BUR', 	'CAM', 	'CAP', 	'CUM', 	'ESS',\
      'GLO', 	'HUD', 	'HUN', 	'MER', 	'MID', 	'MON', 	'MOR',\
      'OCE', 	'PAS', 	'SAL', 	'SOM', 	'SUS', 	'UNI', 	'WAR',\
      'NYC',    'PHL',  'BUC',  'SOU',  'NOR',  'WES',  'ROC']

counties = ["Atlantic", "Bergen", "Burlington", "Camden" , "Cape May",\
            "Cumberland", "Essex", "Gloucester", "Hudson", "Hunterdon" ,\
            "Mercer", "Middlesex" , "Monmouth", "Morris", "Ocean", "Passaic", \
            "Salem", "Somerset", "Sussex", "Union" , "Warren"]

M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"
tours_path = 'Data\\Tours\\'
#read in tour representations
tourReprReader = csv.reader(open(M_PATH+tours_path+'TourRepresentation.csv'),
                            delimiter=',')
tourMat = [row[0] for row in tourReprReader]


#read in neighboring counties
neighborCountyReader = csv.reader(open(M_PATH+tours_path+'neighboringCounties.csv'))
head = neighborCountyReader.next()
neighborMat = [row[3:] for row in neighborCountyReader]
####### Read in files #########
listOfLists = []
employerPath = 'Data\\patronEmployee\\'
for countyNum, county in enumerate(counties):
    f = open(M_PATH+employerPath+county+'.csv', 'rb')
    patronageReader = csv.reader(f, delimiter=',')
    patronageReader.next()
    countyPatronList = []
    for rowNum, prow in enumerate(patronageReader):
        if prow[10]!='0' and prow[10]!='#N/A':
            countyPatronList.append([(countyNum*2)+1, rowNum, float(prow[10]),
                                      float(prow[16]), float(prow[17]), int(prow[8])])
    f.close()
    listOfLists.append(countyPatronList)

####### Run #########
RandomDrawVector = [] #altered globally
RDVIndex = [] #altered globally
block = [0,0] #altered globally
path4 = M_PATH + 'Output\\Module 4\\'
path5 = M_PATH + 'Output\\Module 5\\'
startTime = datetime.now()
for countyNum, ID in enumerate(IDs):
    print(ID + " started at " + str(datetime.now())
          + "total duration so far: " + str(datetime.now()-startTime))
    if countyNum<21:
        snbList = populate_neighboring_businesses(countyNum)
    else:
        snbList = []
    mat = []
    #if countyNum!=0:
    f = open(path4 + str(ID + 'Module4NN.csv'), 'rb')
    personReader = csv.reader(f, delimiter=',')
    mat = [row for row in personReader]
    f.close()
    print 'Read mat'
    co = 0
    for row in mat:
        if co%1000==0:
            print co
        co+=1
        travelerType = int(row[25])
        tourType = int(row[26])
        row.extend(create_tour(tourType, travelerType,
                    listOfLists, snbList, row))
    g = open(path5 + str(IDs[countyNum] + 'Module5NN.csv'), 'wb')
    personWriter = csv.writer(g, delimiter=',')
    personWriter.writerows(mat)
    g.close()

