'''
New Jersey Person Trip Synthesizer

Module 1: Assign work industry and place to Non-NJ workers in NJ
		  '''

__author__      = "Talal Mufti <talalmufti@gmail.com>"
__version__     = "0.40a"
__date__        = "12/31/2012"
__license__     = "python"

import csv
import random as rd
import numpy as np
from numpy import *
import math as m
from datetime import datetime

def expand_distribution(dist):
    'returns list that repeats each element index of dist "freq" times where "freq" is the value of that element'
    vec = [[i]*int(round(x)) for i, x in enumerate(dist)]
    return [num for elem in vec for num in elem]

def normalize_by_average(l, nzl):
    'l must be numpy ndarray. nzl is #non-zero elements in l. returns normalized vector'
    avg = sum(l)/nzl
    l = (l/avg+.5)
    return trunc(l)

def select_one(l):
    'way to randomly select a single item from list. especially convenient for binary dist (True/False, fam/non-fam, under/over 18)'
    '''creates a list that repeats each element	index of dist freq times where freq is the value of that element the chooses one uniformly
    note that when an index has freq zero, it is not represented at all, which causes a problem whern there are only two indeces, one with freq 0'''
    r = np.random.randint(0,len(l)) if len(l)>1 else 0 # added else for when len(l) = 0
    return l[r]

def convert_state_county(s, c):
    ## Credit to Akshay for his code which I based some of this method on
    if s == 34:
        return (c+1)/2 #converts real nj county codes to our numbering
    elif s== 36:
        if (c in NYCountyToID.keys()):
            return NYCountyToID[c]
        else:
            return 27
    if s == 42:
        if c == 101:
            return 22
        else:
             return 23
    elif s in stateToID.keys():
        return stateToID[s]
    else:
        return 100 ##error message

def get_work_industry(county, sex, income):
    'returns Industry code from 0-19'
    if county == -1 or county == 100:
        return -1
    if county in [21,22,23,24,25,26,27,28]:
        return -2
    if sex:
        freqDist = workIndustryMales[county]
        medIncDist = workIndustryIncomesMale[county]
    else:
        freqDist = workIndustryFemales[county]
        medIncDist = workIndustryIncomesFemale[county]
    drawList = freqDist/(medIncDist-income)**2
    ### regular expand_distribution and select_one is too memory intensive in this case
    ###
    drawListCumul = build_cumul_dist(drawList)
    U = random.uniform(0,1)
    k = binary_select(drawListCumul.keys(), U, 0, len(drawListCumul)-1)
    industry = drawListCumul[k]
    return industry

def build_cumul_dist(freqList):
    'build a cumulative distribution from a frequency distribution'
    d = {}
    cum = []
    runTotal=0
    for i, f in enumerate(freqList):
        runTotal+=f
        if i == 0:
            cum.append(f)
        else:
            cum.append(f+cum[i-1])
    for j in range(len(cum)-1, -1, -1):
        d[float(cum[j])/runTotal]=j
    np.sort(d)
    return d

def binary_select(d, value, low, high): # problems when there are zero values, dealt with in build cum
    'Performs binary search to find where a random uniform number between 0 and 1 falls'
    if high <= low+1:
        return d[high]
    mid = (low+high)/2
    if d[mid] > value:
        return binary_select(d, value, low, mid)
    elif d[mid] < value:
        return binary_select(d, value, mid, high)
    else:
        return d[mid]

def scale_worker_spots(c, cNum):
    spots = 0
    l = open(M_PATH+employerPath+c+'.csv', 'rb')
    employerReader = csv.reader(l,delimiter=',')
    header = employerReader.next()
    for rowNum, row in enumerate(employerReader):
        spots+=int(row[13])


    j = open(path1 + str(IDs[cNum] + 'Module1NN.csv'), 'rb')
    personReader = csv.reader(j , delimiter= ',')
    workers = 0
    for rowNum, row in enumerate(personReader):
        if int(row[8]) in [2,4,5]:
            workers+=1
    scales = float(workers)/spots + 1
    return scales

def populate_industry_employers(c, scale):
    'break employer file into lists by industry for a single county c'
    #### Employer by Industry lists to be populated from employer files
    #format:
    #[[#workers], [company names], [zip codes], [3-digit NAICS], [lats], [lons], [pointers]]
    ind0 = [[],[], [], [], [], [], []]
    ind1 = [[],[], [], [], [], [], []]
    ind2 = [[],[], [], [], [], [], []]
    ind3 = [[],[], [], [], [], [], []]
    ind4 = [[],[], [], [], [], [], []]
    ind5 = [[],[], [], [], [], [], []]
    ind6 = [[],[], [], [], [], [], []]
    ind7 = [[],[], [], [], [], [], []]
    ind8 = [[],[], [], [], [], [], []]
    ind9 = [[],[], [], [], [], [], []]
    ind10 = [[],[], [], [], [], [], []]
    ind11 = [[],[], [], [], [], [], []]
    ind12 = [[],[], [], [], [], [], []]
    ind13 = [[],[], [], [], [], [], []]
    ind14 = [[],[], [], [], [], [], []]
    ind15 = [[],[], [], [], [], [], []]
    ind16 = [[],[], [], [], [], [], []]
    ind17 = [[],[], [], [], [], [], []]
    ind18 = [[],[], [], [], [], [], []]
    ind19 = [[],[], [], [], [], [], []]

    indList = [ind0,ind1,ind2,ind3,ind4,ind5,ind6,ind7,ind8,ind9,ind10,ind11,
                ind12,ind13,ind14,ind15,ind16,ind17,ind18,ind19]


    f = open(M_PATH+employerPath+c+'.csv', 'rb')
    employerReader = csv.reader(f,delimiter=',')
    employerReader.next()
    for rowNum, row in enumerate(employerReader):
        if int(row[8][:2]) in industries.keys():
            i = industries[int(row[8][:2])]
            ran =[13,0,1,8,16,17, -1]
            for j in range(len(ran)):
                if j==0:
                    n =int(row[ran[j]])*scale
                    indList[i][j].append(int(n))
                elif j==3:
                    indList[i][j].append(int(row[ran[j]]))
                elif j==4 or j==5:
                    indList[i][j].append(float(row[ran[j]]))
                elif j==6:
                    indList[i][j].append(rowNum+2)
                else:
                    indList[i][j].append(row[ran[j]])
    return indList

def distance(LatS, LonS, LatH, LonH):
    return m.sqrt((LatH-LatS)**2 + (LonH-LonS)**2)

def get_employer(employerList,wC, wI, lat, lon):
    'returns list of employer information for the one it has drawn'
    if wI == -1:
        return [-1]*6
    else:
        el = employerList[wC][wI]
        latlons = [(float(employerList[wC][wI][4][i]),float(j))
                     for i, j in enumerate(employerList[wC][wI][5])]
        distances = np.array([distance(lat,lon, latlon[0],latlon[1])
                    for latlon in latlons])
        p = np.array(el[0])/distances
        ele = expand_distribution(normalize_by_average(p, i+1))
        e = select_one(ele)
        r = [el[x][e] for x in [1,2,3,4,5,6]] # 6 for rowNum, ie pointer
##        except Exception:el
##            print e
##            print employerList
##            print employerList[1][e]
##            print 'HIT STOP'
        return r

########
NonNJIDs = ['NYC', 'PHL', 'BUC', 'SOU', 'NOR', 'WES', 'ROC']

NonNJlatlons = [(40.748716,-73.986171), (39.952335,-75.163789),
(40.229275,-74.936833), (39.745833,-75.546667), (40.608431,-75.490183),
(41.033986,-73.76291), (41.148946,-73.983003), (40.750580,-73.993580)]

counties = ["Atlantic", "Bergen", "Burlington", "Camden" , "Cape May",\
            "Cumberland", "Essex", "Gloucester", "Hudson", "Hunterdon" ,\
            "Mercer", "Middlesex" , "Monmouth", "Morris", "Ocean", "Passaic", \
            "Salem", "Somerset", "Sussex", "Union" , "Warren"]

IDs = ['ATL', 	'BER', 	'BUR', 	'CAM', 	'CAP', 	'CUM', 	'ESS',\
      'GLO', 	'HUD', 	'HUN', 	'MER', 	'MID', 	'MON', 	'MOR',\
      'OCE', 	'PAS', 	'SAL', 	'SOM', 	'SUS', 	'UNI', 	'WAR',\
      'NYC',    'PHL',  'BUC',  'SOU',  'NOR',  'WES',  'ROC']

M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"

path1 = M_PATH + 'Output\\Module 1\\'
path2 = M_PATH + 'Output\\Module 2\\'
respath = 'flowdata\\resdata.txt'
resflow = np.loadtxt(M_PATH+respath, dtype = int, delimiter=',',
                     usecols=[0, 1, 5,6, 10])
wIndPath = 'Data\\Industry\\ACS_10_3YR_S2403_with_ann.csv' # dashes in table replaced with 0.01
workIndustryMales = np.loadtxt(M_PATH+wIndPath, delimiter = ',', skiprows=7,
                    usecols = range(28,89,12)+
                    [112,124,136,160,172,196,208,220,244,256,280,292,304,316])

workIndustryIncomesMale = np.loadtxt(M_PATH+wIndPath, delimiter = ',',
                                    skiprows=7, usecols = range(34, 95, 12) +
                                    [118, 130, 142, 166, 178, 202, 214, 226,
                                     250, 262, 286, 298, 310, 322])
workIndustryFemales = np.loadtxt(M_PATH+wIndPath, delimiter = ',',
                                    skiprows=7, usecols = range(30,91,12)+
                                    [114, 126, 138, 162, 174, 198, 210, 222,
                                     246, 258, 282, 294, 306, 318])
workIndustryIncomesFemale= np.loadtxt(M_PATH+wIndPath, delimiter = ',',
                                    skiprows=7, usecols = range(36,97, 12)+
                                    [120, 132, 144, 168, 180, 204, 216, 228,
                                     252, 264, 288, 300, 312, 324])

employerPath = 'Data\\patronEmployee\\'

industries = {          #First two NAICS code digits to our own coding
                11:0,
                21:1,
                22:7,
                23:2,
                31:3,
                32:3,
                33:3,
                42:4,
                44:5,
                45:5,
                48:6,
                49:6,
                51:8,
                52:9,
                53:10,
                54:11,
                55:12,
                56:13,
                61:14,
                62:15,
                71:16,
                72:17,
                81:18,
                92:19
                }


####### Run ########   TODO Test

employersByIndustry = []
for countyNum, county in enumerate(counties):
    print 'calculating scaling factor for county workers'
    workerScales = scale_worker_spots(county, countyNum)
    print 'populating employers by industries by counties for ' + county
    employersByIndustry.append(populate_industry_employers(county, workerScales))

startTime = datetime.now()
for idNum, ID in enumerate(NonNJIDs):
    print(ID + " started at " + str(datetime.now())
      + " duration: " + str(datetime.now()-startTime))
    f = open(path1 + str(ID + 'Module1NN.csv'), 'rb')
    g = open(path2 + str(ID + 'Module2NN.csv'), 'wb')
    personReader = csv.reader(f , delimiter= ',')
    personWriter = csv.writer(g , delimiter= ',')
    co = 0
    for row in personReader:
        if co%10000 == 0:
             print co
        wc = (int(row[11])-1)/2
        co+=1
        wInd = get_work_industry(wc, row[7], int(row[10]))
        empl = get_employer(employersByIndustry,wc,wInd,
                            NonNJlatlons[idNum][0],NonNJlatlons[idNum][1] )
        personWriter.writerow(row + [wInd] + empl)
    f.close()
    g.close()
