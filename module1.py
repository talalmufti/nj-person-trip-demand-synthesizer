'''
New Jersey Trip File Generator

Module 1: Create residents within households for each block in every county,
          giving each resident: ID, lat/long, county, sex, age, household #,
          household type, traveler type, and income

		  Decided to code in a procedural script style rather
          than object-oriented
		  '''
__author__      = "Talal Mufti <talalmufti@gmail.com>"
__version__     = "0.60a"
__date__        = "8/1/2012"
__license__     = "python"

import csv
import random as rd
import numpy as np
from numpy import *
import math as m
from datetime import datetime

counties = ["Atlantic", "Bergen", "Burlington", "Camden" , "Cape May",\
            "Cumberland", "Essex", "Gloucester", "Hudson", "Hunterdon" ,\
            "Mercer", "Middlesex" , "Monmouth", "Morris", "Ocean", "Passaic", \
            "Salem", "Somerset", "Sussex", "Union" , "Warren"]
countyCodes = range(1,43,2)
countyDic = dict(zip(counties, countyCodes))

IDs = ['ATL', 	'BER', 	'BUR', 	'CAM', 	'CAP', 	'CUM', 	'ESS',\
      'GLO', 	'HUD', 	'HUN', 	'MER', 	'MID', 	'MON', 	'MOR',\
      'OCE', 	'PAS', 	'SAL', 	'SOM', 	'SUS', 	'UNI', 	'WAR',\
      'NYC',    'PHL',  'BUC',  'SOU',  'NOR',  'WES',  'ROC']


ageRanges = [(0,4) , (5,9) , (10,14) , (15,17), (18,19), (20,20), (21,21), (22,24), (25,29), (30,34),\
			(35,39), (40,44), (45,49), (50,54), (55,59), (60,61), (62,64), (65,66), (67,69) , (70, 74), (75,79), (80,84), (85,100) ]

INCOME_BRACKETS = {     1: (0, 9999),
                        2: (10000,14999),
                        3: (15000,24999),
                        4: (25000,34999),
                        5: (35000,49999),
                        6: (50000,74999),
                        7: (75000,99999),
                        8: (100000,149999),
                        9: (150000,199999),
                        10:(200000,1000000)}

# Important indices
HH_DIST = range(6,13)
M_AGE_DIST = range(14,37)
F_AGE_DIST = range(38, 61)
SEX_DIST = [13, 37]
HH_REL_DIST = range(61,86)
FAM_NOFAM = [17,2] #within HH_REL_DIST
UNDER_OVER_EIGHTEEN = range(86,88)
GROUP_QUARTERS = range(88, 130)
#HU100 = 6

#Global Counters
HH_COUNT = 0
PERSON_COUNT = 0


#Global person lists
mchildren = []
fchildren = []
madults = []
fadults = []
pop = 0
poptwo = 0




#main path on my pc - change as appropriate
M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"

def read_census_matrix(county):
    'Reads in Census data for a particular county. Does not include income data'
###SUMLEV, COUNTY, TRACT, BLOCK, LAT, LON, HU100, AREALAND, AREAWATER, H1301 - H1308, P12002 - P12049 ###
    censusLoc = M_PATH + 'Data\\Census 2010\\Census Matrices\\' + county + 'CensusMatrix.txt'
    return np.loadtxt(censusLoc, dtype=int , delimiter=",",usecols =range(4) + range(6, 132), converters = {0:eval,1:eval, 2:eval, 3:eval})

def read_lat_lons(county):
    'Reads in latitudes and longitudes of every block in county'
    censusLoc = M_PATH + 'Data\\Census 2010\\Census Matrices\\' + county + 'CensusMatrix.txt'
    return np.loadtxt(censusLoc,delimiter=",",usecols =(4,5), converters = {4:eval, 5:eval})

def convert_tract(x):
    'Convert track field from income file to its representation in census data'
    return int(x[5:11])
def read_income_matrix():
    'Reads in ACS2010 5 yr'
    incomeLoc = M_PATH + 'Data\\ACS2010 5yr\\ACS_10_5YR_S1901_with_ann_mod.csv'
    faminco = np.loadtxt(incomeLoc, delimiter="," , skiprows=7, usecols =range(15,95,8))
    nonfaminco = np.loadtxt(incomeLoc, delimiter="," , skiprows=7, usecols =range(19,99,8))
    #note that the numbers 15 and 19 above are 2 more than what would seem right
    #when viewing the csv in excel. this is because of commas in the name field
    #loadtxt is ignoring the quotes that make strings and reading the commas as usual
    #not as a part of a string
    tra = np.loadtxt(incomeLoc, dtype=int, delimiter="," , skiprows=7, usecols =[1], converters = {1:convert_tract})
    return tra,faminco, nonfaminco

def get_age(g=-1):
    'random integer within the gth age range. -1 is a failsafe that might not be used '
    #debug
	#print "getting age"
    return rd.randint(ageRanges[g][0], ageRanges[g][1]) if g!=-1 else -1

def populate_residents(maleAgeGroup, femaleAgeGroup):
    'populates 4 global lists of male and female, adults and children. each person represented by a tuple (age, sex) where 1 = male and 0 = female'
    #mpeeps = []
    global mchildren
    global madults
    global fchildren
    global fadults
    global poptwo

    for i, agepop in enumerate(maleAgeGroup): # i is age range
        poptwo+=agepop
        for j in range(agepop): # age pop is no. of residents in that range;
            x = get_age(i)
            if x<=22:
                mchildren.append([x,1,-1]) # [age, male, dummy var]
            else:
                madults.append([x,1,-1])

    for i, agepop in enumerate(femaleAgeGroup):
        poptwo+=agepop
        for j in range(agepop):
    	    x = get_age(i)
    	    if x<=22:
                    fchildren.append([x,0,-1])
    	    else:
                    fadults.append([x,0,-1])

def expand_distribution(dist):
    'returns list that repeats each element index of dist "freq" times where "freq" is the value of that element'
    #print rd.sample(dist, 3)
    vec = [[i]*int(round(x)) for i, x in enumerate(dist)]
    return [num for elem in vec for num in elem]

def select_one(l):
    'way to randomly select a single item from list after expanding it. especially convenient for binary dist (True/False, fam/non-fam, under/over 18)'
    ''' note that when an index has freq zero, it is not represented at all, which causes a problem whern there are only two indeces, one with freq 0'''
	##vec = [[i]*x for i, x in enumerate(dist)]
	##l = [num for elem in vec for num in elem]
    r = np.random.randint(0,len(l)) if len(l)>1 else 0 # added else for when len(l) = 0
    return l[r]

def traveler_type(age, hht):
    'Choose Traveler Type based on age and household type'
    ##0:DNT:0-5, 79+ and those in correct. fac, juvee, nursing homes, other, and military quarters
    ##1:SCN:5-18: 6-15, 16-18*97%
    ##2:SCW:16-18*3%
    ##3:CNT:18-22*90.34% + in Dorms
    ##4:CCW:18-22*9.66% (work in same county)
    ##5:TTT:22-64*78%
    ##6:HWT:22-64*22% + 65-79 #unemployed(~10%) + work-at-home (~8%) +sickdays

    temp = rd.uniform(0,1)
    if age >= 0 and age< 5 or age>79 or hht in [2,3,4,5,7]:
        travelType=0
    elif age>=5 and age<=15:
        travelType=1
    elif age>=16 and age<= 18:
        if temp<=0.97:
            travelType=1
        else:
            travelType=2
    elif age>=18 and age<=22 or hht == 6:
        if temp<=.9034:
            travelType=3
        else:
            travelType=4
    elif age>=22 and age<=64:
        if temp<=.78:
            travelType=5
        else:
            travelType=6
    else:
        travelType=6 ##65-79
    return travelType

def household_helper(r):
    'filters data row into convenient lists (some redundnacy for the sake of clarity), loops and passes them to get_household'
    #household (fam/non fam)
    global pop
    censusPop = sum(r[13:38])
    pop+=censusPop #TODO why does pop come out much higher?
    ####print "r: " + str(r)
    ####print r.shape
    rel = [r[x] for x in HH_REL_DIST]
    ####print "rel: " + str(rel)
    ####print len(rel)
    underover = [r[x] for x in UNDER_OVER_EIGHTEEN]
    underoverExp = expand_distribution(underover)
    fnf = [rel[x] for x in FAM_NOFAM]
    fnfe = expand_distribution(fnf)
    singleSexList = [rel[x] for x in [19, 22]]
    singleSexListExp = expand_distribution(singleSexList)
    householderSexList = [rel[x] for x in [4,5]]
    householderSexListExp = expand_distribution(householderSexList)
    familyRelList = [rel[x] for x in range(6,17)]
    familyRelListExp = expand_distribution(familyRelList)
    sexDistList = [r[x] for x in SEX_DIST]
    sexDistListExp = expand_distribution(sexDistList)
    nonFamSexNotAlone = [rel[x] for x in [20, 23]] # P0290021 and 24
    nonFamSexNotAloneExp = expand_distribution(nonFamSexNotAlone)
    sizeDist = [r[x] for x in HH_DIST]
    sizeDistExp = expand_distribution(sizeDist)

    hh = []

    if True in [x!=0 for x in sizeDist]: #ignore plus in 7+
        while censusPop>0:
            g = select_one(sizeDistExp)
            i = min(censusPop, g+1) #HH size randomly drawn or whatever is left
            #print 'HH size: ' + str(i+1)
            hh.append(get_household(i+1, fnfe, singleSexList, singleSexListExp,
                    householderSexList, householderSexListExp,
                    familyRelList, familyRelListExp,
                    underoverExp, sexDistListExp,
                    nonFamSexNotAlone, nonFamSexNotAloneExp))
            censusPop-=i
##    print 'CensusPop After: ' + str(censusPop)
##    print hh
    return hh

def get_household(s, fe, ssl, ssle, hhsl, hhsle, frl, frle, uoe, sde, nfsna
                  ,nfsnae):
    ' creates a household of size s, with relations based on rel and then populates them from res.'
    #uses dummy info for persons:
    #at index 0: age, 5 for child, 30 for adult
    #at index 1: gender, 2 for when not actually chosen, otherwise 0/1 as usual
    #at index 2: houshold types: -1 for not yet chosen (default),
    #household types: 0 for family, 1 for nonfam
    #see group_quarters for complete list of household types
    h = []
    global mchildren
    global madults
    global fchildren
    global fadults
    #global HH_COUNT
    #HH_COUNT+=1
    fam = select_one(fe) #value of 0 or 1 - corresponds to not-fam/fam
    for i in range(s):
            if s == 1 and True in [x!=0 for x in ssl]: #single person household
                    singleSex = select_one(ssle)
                    h.append((30,singleSex, 1))
            elif(fam and i==0 and  True in [x!=0 for x in hhsl]): #s>1. make main HHer
                    singleSex = select_one(hhsle)
                    h.append((30,singleSex, 0))
            elif(fam and True in [x!=0 for x in frl]):
                    #l = expand_distribution([rel[x] for x in range(4,17)])
                    person = select_one(frle)
                    #if in this list, add this, if in that list, add that,
                    #break up some by age proportions from P16
##                    if (person == 0):
##                        h.append((30,0,0)) #TODO THE HECK IS THIS? no repeated HHers
##                    if (person ==1):
##                        h.append((30,1,0))
                    if(person == 0): #spouse
                        h.append((30, int(not singleSex), 0))# same sex
                                            #spouses are NOT counted as
                                            #spouses in the 2010 census
                    elif(person in [1,2,3,4]):
                        h.append((5, 2, 0))
                    elif(person in [6,7,8]):
                        h.append((30,2,0))
                    elif(person in [5,9,10]):
                        if (select_one(uoe)):
                            h.append((5, 2, 0))
                        else:
                            h.append((30,2,0))

            elif(True in [x!=0 for x in nfsna]):
                person = select_one(nfsnae)
                if (person == 0):
                    h.append((30, 1,1))
                elif(person == 1):
                    h.append((30, 0,1))
                elif(True in [x!=0 for x in ssl]):
                    if rd.random()<.75:
                        h.append((30, select_one(ssle),1)) # assumes non-relative is adult
                    else:
                        h.append((5, select_one(ssle),1))
    peeps = []
    for p in h:
        if p[0:2] == (30,2):
            if select_one(sde) and len(fadults)>0:
                fTemp = fadults.pop()
                fTemp[2] = p[2] #so there's no need to do separate fam/non fam assignments
                peeps.append((fTemp))
            elif len(madults)>0:
                mTemp = madults.pop()
                mTemp[2] = p[2]
                peeps.append((mTemp))
        elif p[0:2] == (30,0) and len(madults)>0:
            mTemp = madults.pop()
            mTemp[2] = p[2]
            peeps.append((mTemp))
        elif p[0:2] == (30,1) and len(fadults)>0:
            fTemp = fadults.pop()
            fTemp[2] = p[2]
            peeps.append((fTemp))
        elif p[0:2] == (5,2):
            if select_one(sde) and len(fchildren)>0:
                fTemp = fchildren.pop()
                fTemp[2] = p[2]
                #print fTemp
                peeps.append((fTemp))
            elif len(mchildren)>0:
                mTemp = mchildren.pop()
                mTemp[2] = p[2]
                #print mTemp
                peeps.append((mTemp))
                    #a = fchildren.pop() if select_one([row[x] for x in SEX_DIST]) and len(fchildren)>0 else mchildren.pop()
                    #peeps.append(a)
    #print "new row.  Remaining children"
    #print mchildren
    #print fchildren
    #print " same row. Remaining Adults"
    #print madults
    #print fadults
    #print 'peeps'
    #print peeps
    return peeps

def get_group_quarters(r):
    'account for group quarters like college dorms and military quarters'
    ##Household types:
    ## 0: Family
    ## 1: Non-family
    ## 2: Correctional Facilities
    ## 3: Juvenile Detentions
    ## 4: Nursing homes
    ## 5: Other institutionalized quarters
    ## 6: Student housing
    ## 7: Military
    ## 8: Other non institutionalized quarters
    global mchildren
    global madults
    global fchildren
    global fadults
    cfa = [] #2 correctional fac. for adults
    j = [] #3
    nh = [] #4
    oiq = [] #5
    sh = [] #6
    m = [] #7
    oniq = [] #8
    l = [cfa,j,nh,oiq,sh,m,oniq]
    gqlist = [r[x] for x in GROUP_QUARTERS]
    for i, gqsize in enumerate(gqlist):
        mod = i%7
        if i in range(0,7):
            popList = mchildren
            popRange = (14, 17)
        elif i in range(7,14):
            popList = madults
            popRange = (18, 64)
        elif i in range(14,21):
            popList = madults
            popRange = (65,120)
        elif i in range(21, 28):
            popList = fchildren
            popRange =(14, 17)
        elif i in range(28,35):
            popList = fadults
            popRange = (18, 64)
        elif i in range(34,42):
            popList = fadults
            popRange = (65,120)
        for j in range(gqsize):
            pll = len(popList)
            if pll>0:
                for c in range(pll):
                    popped = popList.pop()
                    if popped[0]>=popRange[0] and popped[0]<=popRange[1]:
                        break
                    else:
                        popList.insert(0, popped)
                        popped = -1
                if popped == -1:
                    break
                else:
                    popped[2] = mod+2
                    l[mod].append(popped)
    return l

def get_hh_income(famIncome, nonFamIncome, hht):
    'generate an income for a household'
    if hht:
        i = nonFamIncome
    else:
        i = famIncome
    ie = expand_distribution(i)
    bracket = select_one(ie) + 1
    if bracket == 1:
        amount = rd.triangular(2000, 10000,7500)
    else:
        amount = rd.uniform(INCOME_BRACKETS[bracket][0], INCOME_BRACKETS[bracket][1])
    return amount

def add_individual_income_tt(hhi, h):
    'generate individual incomes for members of a household h'
    hhinctt = []
    l = 0 #number of non-student income earners
    for p in h: #for person in household
        tt = traveler_type(p[0], 0)
        if tt in[5,6]:
            hhinctt.append([tt,-1,0])
            l+=1
        elif tt in [0,1,3]:
            hhinctt.append([tt,0,0])
        elif tt in[2,4]:
            studentInc = rd.uniform(INCOME_BRACKETS[1][0],
                                    min(INCOME_BRACKETS[1][1],hhi))
            hhinctt.append([tt, 1, int(studentInc)])
            hhi-=studentInc
    coeffs = []
    for i in xrange(l):
        coeffs.append(rd.random())
    s = sum(coeffs)
    indincomes = [hhi*c/s for c in coeffs]
    for q in hhinctt:
        if q[1] == -1:
            inc = indincomes.pop()
            q[1] = income_amount_to_code(inc)
            q[2] = int(inc)
    return hhinctt

def income_amount_to_code(income):
    'returns the appropriate income bracket code for a given income'
    # 0  No Income
    # 1  Less than $10,000
    # 2  $10,000 to $14,999
    # 3  $15,000 to $24,999
    # 4  $25,000 to $34,999
    # 5  $35,000 to $49,999
    # 6  $50,000 to $74,999
    # 7  $75,000 to $99,999
    # 8  $100,000 to $149,999
    # 9  $150,000 to $199,999
    # 10 $200,000 or more
    for k in INCOME_BRACKETS.keys():
        if income<=INCOME_BRACKETS[k][1] and income>=INCOME_BRACKETS[k][0] and income != 0:
            return k
        elif income == 0:
            return 0

def person_writer(pW, c, l, gql, ll, fi, nfi):
    'write residents in households to file and append person and household ids'
    ##  county number 1:41, house id, hh type, lat, lon, person id, age, sex,
    ##  travel type, income bracket, income amount
    global HH_COUNT
    global PERSON_COUNT
    for  i, house in enumerate(l):
        if len(house) != 0:
            HH_COUNT+=1
            hhIncome = get_hh_income(fi, nfi, house[0][2])
            indIncome = add_individual_income_tt(hhIncome, house)
            for j, p in enumerate(house):
                PERSON_COUNT+=1
                idnum = str(100000000 + PERSON_COUNT)
                pid = IDs[c] + idnum[1:]
                pW.writerow([countyCodes[c]] + [HH_COUNT] + [p[2]] + [ll[0]] + [ll[1]]
                + [pid] + [p[0]] + [p[1]]
                + [indIncome[j][0]] + [indIncome[j][1]] + [indIncome[j][2]])
    for k, quarter in enumerate(gql):
        if len(quarter) != 0:
            HH_COUNT+=1
            for z, q in enumerate(quarter):
                PERSON_COUNT+=1
                idnum = str(100000000 + PERSON_COUNT)
                pid = IDs[c] + idnum[1:]
                income = 0
                pW.writerow([countyCodes[c]] + [HH_COUNT] + [q[2]] + [ll[0]] + [ll[1]]
                + [pid] + [q[0]] + [q[1]]
                + [traveler_type(q[0], q[2])] + [income] + [income])


'create residents, give them attributes (age, sex, worker type) and place them in households'
startTime = datetime.now()
tracts, fincome, nfincome = read_income_matrix()

for countyNum, county in enumerate(counties):
    print(county + " started at " + str(datetime.now())
      + " duration: " + str(datetime.now()-startTime))

    path = M_PATH + 'Output\\Module 1\\december\\'
    HH_COUNT = 0
    PERSON_COUNT = 0
    f = open(path + str(IDs[countyNum] + 'Module1.csv'), 'wb')
    personWriter = csv.writer(f , delimiter= ',')
    census = read_census_matrix(county)

    latlon = read_lat_lons(county)
    tractOld = -1
    k = 0
    acsTract = tracts[k]
    for rownum , row in enumerate(census):
        tract = row[2]
        if tract != acsTract:
            k+=1 #this will break if the unique lists are not the same and in the right order
            acsTract = tracts[k]
        finc = fincome[k]
        nfinc = nfincome[k]
        populate_residents([row[x] for x in M_AGE_DIST],
                       [row[x] for x in F_AGE_DIST])
        rd.shuffle(mchildren)
        rd.shuffle(madults)
        rd.shuffle(fchildren)
        rd.shuffle(fadults)
        gqlist = get_group_quarters(row)
        hhh = household_helper(row)
        person_writer(personWriter, countyNum, hhh, gqlist, latlon[rownum], finc, nfinc) #TODO add geoid and census block number
    f.close()
    print(datetime.now()-startTime)


