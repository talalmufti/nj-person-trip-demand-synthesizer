'''
New Jersey Person Trip Synthesizer

Module 6:   Adds temporal dimension to model by drawing from asymmetric 'bell time'
            distributions.
		  '''
__author__      = "Talal Mufti <talalmufti@gmail.com>"
__version__     = "0.10a"
__date__        = "8/24/2012"
__license__     = "python"

import csv
import random as rd
import numpy as np
from numpy import *
import math as m
from datetime import datetime

EARTH_RAD  = 3963.17

def great_circle_distance(LatS, LonS, LatH, LonH):
    dLat = (LatS-LatH)*m.pi/180
    dLon = (LonS-LonH)*m.pi/180
    a = (m.sin(dLat/2) * m.sin(dLat/2) + m.cos(LatH*pi/180) *
         m.cos(LatS*pi/180) * m.sin(dLon/2) * m.sin(dLon/2))
    c = 2 * math.asin(sqrt(a))
    return EARTH_RAD*c

def get_distances(l):
    global distance
    distance=[]
    distance.append(-1)
    for i in xrange(30,l-6,6):
        if l<34:
            break
        distance.append(great_circle_distance(float(row[i]), float(row[i+1])
                                         ,float(row[i+6]), float(row[i+7])))

def get_times(tR, trT, tt, ind, st ):
    global arrivalTimes
    global departureTimes
    arrivalTimes = [-1]*len(tR)
    departureTimes = [-1]*len(tR)
    for node, char in enumerate(tR):
        if node == 1:
            if char == 'W' and trT!=6:
                mi, ma, mo = workSchedules[ind][0:3]
                arrivalTimes[node] = rd.triangular(mi,ma,mo)
                departureTimes[node-1] = arrivalTimes[node]-distance[node]/woSpeed
                if  tt == 11:
                    departureTimes[node] = rd.triangular(11.917, 13, 12.5)*3600
                elif tt in [4,8]:
                    mi, ma, mo = workSchedules[ind][3:6]
                    departureTimes[node] = rd.triangular(mi-1,ma-1,mo-1)
                else:
                    mi, ma, mo = workSchedules[ind][3:6]
                    departureTimes[node] = rd.triangular(mi,ma,mo)
            elif char == 'S':
                mi, ma, mo = schoolSchedules[st][0:3]
                arrivalTimes[node] = rd.triangular(mi,ma,mo)
                departureTimes[node-1] = arrivalTimes[node]-distance[node]/schSpeed
                mi, ma, mo = schoolSchedules[st][3:6]
                if tt in [3,7]:
                    departureTimes[node] = rd.triangular(mi-2,ma,mo-1)
                else:
                    departureTimes[node] = rd.triangular(mi,ma,mo)
            elif char == 'O' or (char == 'W' and trT==6):
                departureTimes[node-1] = rd.triangular(9,12,11)*3600
                arrivalTimes[node] = departureTimes[node-1] + distance[node]/woSpeed
                departureTimes[node] = arrivalTimes[node] + rd.triangular(.1, 2, .33)*3600
        elif node >= 2:
            if char == 'H':
                arrivalTimes[node] = departureTimes[node-1] + distance[node]/woSpeed
                if node == len(tR) - 1:
                    break
                else:
                    departureTimes[node] = arrivalTimes[node] + rd.triangular(.5,3,1)*3600
            elif char == 'W' and trT!=6:
                arrivalTimes[node] = departureTimes[node-1]+distance[node]/woSpeed
                #print st
                mi, ma, mo = workSchedules[ind][3:6]
                if tt in [3,7] and mi < departureTimes[node]+1:
                        mi+=1
                        ma+=2
                        mo+=1.5
                departureTimes[node] = rd.triangular(mi,ma,mo)
            elif char == 'S':
                arrivalTimes[node] = departureTimes[node-1]+distance[node]/schSpeed
                mi, ma, mo = schoolSchedules[st][3:6]
                departureTimes[node] = rd.triangular(mi+1,ma+4,mo+4)
            elif char == 'O' or (char == 'W' and trT==6):
                arrivalTimes[node] = departureTimes[node-1]+distance[node]/woSpeed
                departureTimes[node] = arrivalTimes[node] + rd.triangular(.1, 2, .33)*3600

M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"
path5 = M_PATH + 'Output\\Module 5\\'
path6 = M_PATH + 'Output\\Module 6\\'
startTime = datetime.now()
M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"
tours_path = 'Data\\Tours\\'
school_path = 'Data\\School\\school schedules by type.csv'
industry_path = 'Data\\Industry\\work schedules by naics.csv'

IDs = ['ATL', 	'BER', 	'BUR', 	'CAM', 	'CAP', 	'CUM', 	'ESS',\
      'GLO', 	'HUD', 	'HUN', 	'MER', 	'MID', 	'MON', 	'MOR',\
      'OCE', 	'PAS', 	'SAL', 	'SOM', 	'SUS', 	'UNI', 	'WAR',\
      'NYC',    'PHL',  'BUC',  'SOU',  'NOR',  'WES',  'ROC']

arrivalTimes = []
departureTimes = []
distance = []

woSpeed = 30.0/3600.0
schSpeed = 15.0/3600.0
##read in Header (2)
headerReader = csv.reader(open(path6+'Header6.csv', 'rb' ))
header = headerReader.next()

##read in Tour Representation
tourReprReader = csv.reader(open(M_PATH+tours_path+'TourRepresentation.csv',
                             'rb'), delimiter=',')
tourMat = [row[0] for row in tourReprReader]

##read in schedules
schoolSchedules = np.loadtxt(M_PATH+school_path, delimiter=',' ,dtype=int)
workSchedules = np.loadtxt(M_PATH+industry_path, delimiter=',' ,dtype=int)

##RUN
for countyNum, ID in enumerate(IDs):
    print(ID + " started at " + str(datetime.now())
      + " duration: " + str(datetime.now()-startTime))
    f = open(path5 + str(ID + 'Module5NN.csv'), 'rb')
    personReader = csv.reader(f, delimiter=',')
    g = open(path6 + str(ID + 'Module6NN.csv'), 'wb')
    personWriter = csv.writer(g, delimiter=',')
    personWriter.writerow(header)
    #mat = [row for row in personReader]
    #f.close()
    #for co, row in enumerate(mat):
    co=0
    for row in personReader:
        if co%10000==0:
            print co
        tourType = int(row[26])
        travelerType = int(row[25])
        industryCode = int(row[12])
        studentType = int(row[19])
        tourRepr = tourMat[tourType]
        get_distances(len(row))
        get_times(tourRepr, travelerType, tourType, industryCode, studentType)
        temp=[]
        for x in xrange(len(tourRepr)):
            temp.extend(row[27+6*x:33+6*x]+[arrivalTimes[x],departureTimes[x],distance[x]])

##        personWriter.writerow(row[0:33]+
##                              [arrivalTimes[0]]+[departureTimes[0]]+distance[0]+
##                              row[33:42]+
##                              [arrivalTimes[1]]+[departureTimes[1]]+distance[1]+
##                              row[42:51]+
##                              [arrivalTimes[2]]+[departureTimes[1]]+distance[1]+
##                              row[42:51]+)
        co+=1
        personWriter.writerow(row[0:27]+temp)
    f.close()
    g.close()

