Author: Talal Mufti
Adviser: Alain L. Kornhauser
Thesis: SYNTHESIS OF SPATIALLY & TEMPORALLY DISAGGREGATE PERSON TRIP DEMAND:
APPLICATION FOR A TYPICAL NEW JERSEY WEEKDAY

From Methodologies:

"Synthesizing Travel Demand across New Jersey
To restate the goals of this project in operational terms, the model creates a population of individuals whose characteristics, together, come to resemble the aggregate characteristics of people who live and/or work in New Jersey. Then for each of those individuals, the model assigns a �Traveler Type� that is representative of individuals with such characteristics and a home that is representative of where people actually live in NJ.  Next, it assigns them work, school and other activities as well as the timings for these functions that are representative of where and when people take part in those respective functions."

The 8 python modules included together form the Synthesizer.