'''
New Jersey Person Trip Synthesizer

Module 2: Building on resident file produced in module 1, adds wokers from other
          states that commute into NJ counties, assigns each working resident a
          work county, an industry, and then based on that and the
          resident's income class, a place of work.
		  '''
__author__      = "Talal Mufti <talalmufti@gmail.com>"
__version__     = "0.40a"
__date__        = "12/31/2012"
__license__     = "python"

import csv
import random as rd
import numpy as np
from numpy import *
import math as m
from datetime import datetime

#main path on my pc
M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"

NonNJIDs = ['NYC', 'PHL', 'BUC', 'SOU', 'NOR', 'WES', 'ROC', 'INTL'] #nyc has id number 21 or 42 after FIPS codes
NonNJlatlons = [(40.748716,-73.986171), (39.952335,-75.163789),
(40.229275,-74.936833), (39.745833,-75.546667), (40.608431,-75.490183),
(41.033986,-73.76291), (41.148946,-73.983003), (40.750580,-73.993580)] #last is International

## Credit to Akshay Kumar for the original version of the matching dictionary below
stateToID = {
             1:  24,
             2:  23,
             4:  23,
             5:  23,
             6:  23,
             8:  23,
             9:  25,
             10:  24,
             11:  24,
             12:  24,
             13:  24,
             15:  23,
             16:  23,
             17:  23,
             18:  23,
             19:  23,
             20:  23,
             21:  23,
             22:  23,
             23:  25,
             24:  24,
             25:  25,
             26:  23,
             27:  23,
             28:  24,
             29:  23,
             30:  23,
             31:  23,
             32:  23,
             33:  25,
             35:  23,
             37:  24,
             38:  23,
             39:  23,
             40:  23,
             41:  23,
             44:  25,
             45:  24,
             46:  23,
             47:  24,
             48:  23,
             49:  23,
             50:  25,
             51:  24,
             53:  23,
             54:  24,
             55:  23,
             56:  23
        }

NYCountyToID = {
                ##new york city
                 61:  21,
                 5:  21,
                 47:  21,
                 81:  21,
                 85:  21,
                 103:  21,
                 59:  21,
                ##westchester
                 119:  26
                 }

#NonNJpops = [86418, 18586, 99865, 13772, 5046, 6531, 32737]


counties = ["Atlantic", "Bergen", "Burlington", "Camden" , "Cape May",\
            "Cumberland", "Essex", "Gloucester", "Hudson", "Hunterdon" ,\
            "Mercer", "Middlesex" , "Monmouth", "Morris", "Ocean", "Passaic", \
            "Salem", "Somerset", "Sussex", "Union" , "Warren"]
countyCodes = range(1,43,2)
countyDic = dict(zip(counties, countyCodes))

IDs = ['ATL', 	'BER', 	'BUR', 	'CAM', 	'CAP', 	'CUM', 	'ESS',\
      'GLO', 	'HUD', 	'HUN', 	'MER', 	'MID', 	'MON', 	'MOR',\
      'OCE', 	'PAS', 	'SAL', 	'SOM', 	'SUS', 	'UNI', 	'WAR',\
      'NYC',    'PHL',  'BUC',  'SOU',  'NOR',  'WES',  'ROC']

industries = {          #First two NAICS code digits to our own coding
                11:0,
                21:1,
                22:7,
                23:2,
                31:3,
                32:3,
                33:3,
                42:4,
                44:5,
                45:5,
                48:6,
                49:6,
                51:8,
                52:9,
                53:10,
                54:11,
                55:12,
                56:13,
                61:14,
                62:15,
                71:16,
                72:17,
                81:18,
                92:19
                }


workFlowPointer = 0
workFlowTrailer = 0

def expand_distribution(dist):
    'returns list that repeats each element index of dist "freq" times where "freq" is the value of that element'
    vec = [[i]*int(round(x)) for i, x in enumerate(dist)]
    return [num for elem in vec for num in elem]

def normalize_by_average(l, nzl):
    'l must be numpy ndarray. nzl is #non-zero elements in l. returns normalized vector'
    avg = sum(l)/nzl
    l = (l/avg+.5) #Prof K suggested l/avg +.5 before truncation, but I prefer this
    return trunc(l)

def select_one(l):
    'way to randomly select a single item from list. especially convenient for binary dist (True/False, fam/non-fam, under/over 18)'
    '''note that when an index has freq zero, it is not represented at all, which causes a problem whern there are only two indeces, one with freq 0'''
    r = np.random.randint(0,len(l)) if len(l)>1 else 0 # added else for when len(l) = 0
    return l[r]

def distance(LatS, LonS, LatH, LonH):
    'L2 Distance function'
    return m.sqrt((LatH-LatS)**2 + (LonH-LonS)**2)

def convert_state_county(s, c):
    'converts state and county to appropriate county code as used in the modules'
    ## Credit to Akshay for his code which I based some of this function on
   # doesn't deal differently with international
    if s == 34:
        return (c-1)/2 #converts real nj county codes to our numbering
    elif s== 36:
        if (c in NYCountyToID.keys()):
            return NYCountyToID[c]
        else:
            return 27
    elif s == 42:
        if c == 101:
            return 22
        else:
             return 23
    elif c==0: #international
        return 28
    elif s in stateToID.keys():
        return stateToID[s]
    else:
        return 100 ##error message


def populate_work_flow_list(countyCode, rflow):
    'helper function to read number of workers commuting into county'
    l=[]
    global workFlowPointer
    global workFlowTrailer
    workFlowTrailer = workFlowPointer
    row = rflow[workFlowPointer]
    while row[1] == countyCode and workFlowPointer<len(rflow)-1:
        #if row[3] != 0:
        l.append(row[4])
        workFlowPointer+=1
        row = rflow[workFlowPointer]
    return expand_distribution(l)

def get_work_county(rescounty, hht, tt, clist):
    'Assign a work county to workers, -1 to nonworkers, and rescounty to student workers'
    if tt in [0,1,3,6] or hht in [2, 3, 4, 5, 7]:
        return -1
    elif tt in [2,4]:
        return rescounty
    else:
        i = select_one(clist)
        return convert_state_county(resflow[workFlowTrailer+i,2],
                                    resflow[workFlowTrailer+i,3])

def get_work_industry(county, sex, income):
    'returns Industry code from 0-19'
    #print county
    if county == -1 or county == 100:
        return -1
    if county in [21,22,23,24,25,26,27,28]:
        return -2
    if sex:
        freqDist = workIndustryMales[county]
        medIncDist = workIndustryIncomesMale[county]
    else:
        freqDist = workIndustryFemales[county]
        medIncDist = workIndustryIncomesFemale[county]
    # check for zeros?
    drawList = freqDist/(medIncDist-income)**2
    ###old way is too memory intensive in this case
    #dle = expand_distribution(10000000*drawList)
    #industry = select_one(dle)
    ###
    drawListCumul = build_cumul_dist(drawList)
    U = random.uniform(0,1)
    k = binary_select(drawListCumul.keys(), U, 0, len(drawListCumul)-1)
    industry = drawListCumul[k]
    return industry

def build_cumul_dist(freqList):
    'create a cumulative distribution from a frequency distribution'
    d = {}
    cum = []
    runTotal=0
    for i, f in enumerate(freqList):
        runTotal+=f
        if i == 0:
            cum.append(f)
        else:
            cum.append(f+cum[i-1])
    for j in range(len(cum)-1, -1, -1):
        d[float(cum[j])/runTotal]=j
    np.sort(d)
    return d

def binary_select(d, value, low, high): # problems when there are zero values, dealt with in build cum
    'Performs binary search to find where a random uniform number between 0 and 1 falls'
    if high <= low+1:
        return d[high]
    mid = (low+high)/2
    if d[mid] > value:
        return binary_select(d, value, low, mid)
    elif d[mid] < value:
        return binary_select(d, value, mid, high)
    else:
        return d[mid]

def scale_worker_spots(c, cNum):
    'calculate scaline factor for number of workers for county c'
    spots = 0
    l = open(M_PATH+employerPath+c+'.csv', 'rb')
    employerReader = csv.reader(l,delimiter=',')
    header = employerReader.next()
    for rowNum, row in enumerate(employerReader):
        spots+=int(row[13])
    j = open(path1 + str(IDs[cNum] + 'Module1NN.csv'), 'rb')
    personReader = csv.reader(j , delimiter= ',')
    workers = 0
    for rowNum, row in enumerate(personReader):
        #if int(row[12]) in xrange(20):
         #   workers[int(row[12])]+=1
        if int(row[8]) in [2,4,5]:
            workers+=1
    scales = float(workers)/spots + 1
    return scales

def populate_industry_employers(c, scale):
    'break employer file into lists by industry for a single county c'
    #### Employer by Industry lists to be populated from employer files
    #format:
    #[[#workers], [company names], [zip codes], [3-digit NAICS], [lats], [lons], [pointers]]
    ind0 = [[],[], [], [], [], [], []]
    ind1 = [[],[], [], [], [], [], []]
    ind2 = [[],[], [], [], [], [], []]
    ind3 = [[],[], [], [], [], [], []]
    ind4 = [[],[], [], [], [], [], []]
    ind5 = [[],[], [], [], [], [], []]
    ind6 = [[],[], [], [], [], [], []]
    ind7 = [[],[], [], [], [], [], []]
    ind8 = [[],[], [], [], [], [], []]
    ind9 = [[],[], [], [], [], [], []]
    ind10 = [[],[], [], [], [], [], []]
    ind11 = [[],[], [], [], [], [], []]
    ind12 = [[],[], [], [], [], [], []]
    ind13 = [[],[], [], [], [], [], []]
    ind14 = [[],[], [], [], [], [], []]
    ind15 = [[],[], [], [], [], [], []]
    ind16 = [[],[], [], [], [], [], []]
    ind17 = [[],[], [], [], [], [], []]
    ind18 = [[],[], [], [], [], [], []]
    ind19 = [[],[], [], [], [], [], []]

    indList = [ind0,ind1,ind2,ind3,ind4,ind5,ind6,ind7,ind8,ind9,ind10,ind11,
                ind12,ind13,ind14,ind15,ind16,ind17,ind18,ind19]


    f = open(M_PATH+employerPath+c+'.csv', 'rb')
    employerReader = csv.reader(f,delimiter=',')
    employerReader.next()
    for rowNum, row in enumerate(employerReader):
        if int(row[8][:2]) in industries.keys():
            i = industries[int(row[8][:2])]
            ran =[13,0,1,8,16,17, -1]
            for j in range(len(ran)):
                if j==0:
                    n =int(row[ran[j]])*scale
                    indList[i][j].append(int(n))
                elif j==3:
                    indList[i][j].append(int(row[ran[j]]))
                elif j==4 or j==5:
                    indList[i][j].append(float(row[ran[j]]))
                elif j==6:
                    indList[i][j].append(rowNum+2)
                else:
                    indList[i][j].append(row[ran[j]])
    return indList

def get_employer(employerList,wC, wI, lat, lon):
    'returns list of employer information for the one it has drawn'
    if wI == -1:
        return [-1]*6
    elif wI == -2:
        lat,lon = NonNJlatlons[(wC-21)]
        return ['Out of State: '+NonNJIDs[(wC-21)], 99999, 999, lat, lon,-1]
    else:
        el = employerList[wC][wI]
        latlons = [(float(employerList[wC][wI][4][i]),float(j))
                     for i, j in enumerate(employerList[wC][wI][5])]
        l = np.array([distance(lat,lon, latlon[0],latlon[1])
                    for latlon in latlons])
        p = np.array(el[0])/l
        ele = expand_distribution(normalize_by_average(p, i+1))
        e = select_one(ele)
        r = [el[x][e] for x in [1,2,3,4,5,6]] # 6 for rowNum, ie pointer
##        except Exception:el
##            print e
##            print employerList
##            print employerList[1][e]
##            print 'HIT STOP'
        return r


####### Read in Files ########
path1 = M_PATH + 'Output\\Module 1\\'
path2 = M_PATH + 'Output\\Module 2\\'
respath = M_PATH + 'Data\\JTW\\resdata.txt'
resflow = np.loadtxt(M_PATH+respath, dtype = int, delimiter=',',
                     usecols=[0, 1, 5,6, 10])
wIndPath = 'Data\\Industry\\ACS_10_3YR_S2403_with_ann.csv' # dashes in table replaced with 0.01
workIndustryMales = np.loadtxt(M_PATH+wIndPath,delimiter = ',',
                                    skiprows=7, usecols = range(30,91,12)+
                                     [114,126,138,162,174,198,210,222,246,
                                     258,282,294,306,318])
                                    #range(28,325, 12))
workIndustryIncomesMale = np.loadtxt(M_PATH+wIndPath, delimiter = ',',
                                    skiprows=7, usecols = range(36, 97, 12) +
                                    [118, 130, 142, 166, 178, 202, 214, 226,
                                     250, 262, 286, 298, 310, 322])
workIndustryFemales = np.loadtxt(M_PATH+wIndPath, delimiter = ',',
                                    skiprows=7, usecols = range(32,93,12)+
                                    [116, 128, 140, 164, 176, 200, 212, 224,
                                     248, 260, 284, 296, 308, 320])
workIndustryIncomesFemale= np.loadtxt(M_PATH+wIndPath, delimiter = ',',
                                    skiprows=7, usecols = range(38,99, 12)+
                                    [120, 132, 144, 168, 180, 204, 216, 228,
                                     252, 264, 288, 300, 312, 324])

employerPath = 'Data\\patronEmployee\\'
employersByIndustry = []
for countyNum, county in enumerate(counties):
    #print county
    print 'calculating scaling factor for county workers'
    workerScales = scale_worker_spots(county, countyNum)
    print 'populating employers by industries by counties for ' + county
    employersByIndustry.append(populate_industry_employers(county, workerScales))

####### Run ########

startTime = datetime.now()
for countyNum, ID in enumerate(IDs):
    if countyNum>20:
        break
    print(ID + " started at " + str(datetime.now())
      + " duration: " + str(datetime.now()-startTime))
    f = open(path1 + str(IDs[countyNum] + 'Module1NNNewRun2.csv'), 'rb')
    g = open(path2 + str(IDs[countyNum] + 'Module2NNNewRun2.csv'), 'wb')
    personReader = csv.reader(f , delimiter= ',')
    personWriter = csv.writer(g , delimiter= ',')
    c = countyCodes[countyNum] # 0-20 => 1-41
    print 'workFlowPointer: ' + str(workFlowPointer)
    print 'workFlowTrailer: ' + str(workFlowTrailer)
    countylist = populate_work_flow_list(c,resflow)
    co = 0
    for row in personReader:
        if co%10000 == 0:
            print co
        wc = get_work_county(countyNum, int(row[2]), int(row[8]), countylist)
        co+=1
        wInd = get_work_industry(wc, int(row[7]), float(row[10]))
        empl = get_employer(employersByIndustry,wc,wInd, float(row[3]), float(row[4]))
        if wc<21:
            wc = wc*2 +1
        else:
            wc+=21
        personWriter.writerow(row + [wc] + [wInd] + empl)
    f.close()
    g.close()



