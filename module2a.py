'''
New Jersey Person Trip Synthesizer

Module 2a:  Create Resident files for workers who commute to NJ from out of state
            giving each commuter: ID, lat/long, county, sex, age, household #,
            household type, traveler type, income, and work county
'''

__author__      = "Talal Mufti <talalmufti@gmail.com>"
__version__     = "0.40a"
__date__        = "12/31/2012"
__license__     = "python"

#The following code has been adapted partially from Natalie Webb's Task1 code
import csv
import random as rd
import numpy as np
from numpy import *

NonNJIDs = ['NYC', 'PHL', 'BUC', 'SOU', 'NOR', 'WES', 'ROC'] #nyc has id number 21
NonNJcounts = []
###############
NYCworkers = []
PHLworkers = []
BUCworkers = []
SOUworkers = []
NORworkers = []
WESworkers = []
ROCworkers = []

NonNJdict = {0:NYCworkers,
             1:PHLworkers,
             2:BUCworkers,
             3:SOUworkers,
             4:NORworkers,
             5:WESworkers,
             6:ROCworkers}
###############
NonNJlatlons = [(40.748716,-73.986171), (39.952335,-75.163789),
(40.229275,-74.936833), (39.745833,-75.546667), (40.608431,-75.490183),
(41.033986,-73.76291), (41.148946,-73.983003)]
###############
#NonNJpops = [86418, 18586, 99865, 13772, 5046, 6531, 32737]
###############
## Credit to Akshay Kumar for the matching dictionary below
stateToID = { ##numbering assumes nj counties numbered 0-20, not the case in our modules
             1:  24,
             4:  23, #yes, there is no 3 in FIPS state numeric code
             5:  23,
             6:  23,
             8:  23,
             9:  25,
             10:  24,
             11:  24,
             12:  24,
             13:  24,
             15:  23,
             16:  23,
             17:  23,
             18:  23,
             19:  23,
             20:  23,
             21:  23,
             22:  23,
             23:  25,
             24:  24,
             25:  25,
             26:  23,
             27:  23,
             28:  24,
             29:  23,
             30:  23,
             31:  23,
             32:  23,
             33:  25,
             35:  23,
             37:  24,
             38:  23,
             39:  23,
             40:  23,
             41:  23,
             44:  25,
             45:  24,
             46:  23,
             47:  24,
             48:  23,
             49:  23,
             50:  25,
             51:  24,
             53:  23,
             54:  24,
             55:  23,
             56:  23
            }

NYCountyToID = {
                ##new york city
                 61:  21,
                 5:  21,
                 47:  21,
                 81:  21,
                 85:  21,
                 103:  21,
                 59:  21,
                ##westchester
                 119:  26
                 }

INCOME_BRACKETS = {     1: (0, 9999),
                        2: (10000,14999),
                        3: (15000,24999),
                        4: (25000,34999),
                        5: (35000,49999),
                        6: (50000,74999),
                        7: (75000,99999),
                        8: (100000,149999),
                        9: (150000,199999),
                        10:(200000,1000000)}
################

def convert_state_county(s, c):
    'converts state and county combinations read from the work flow files into the locations relevant'
    ## Credit to Akshay for his code which I based some of this method on
    if s == 34:
        return (c+1)/2 #converts real nj county codes to our numbering
    elif s== 36:
        if (c in NYCountyToID.keys()):
            return NYCountyToID[c]
        else:
            return 27
    if s == 42:
        if c == 101:
            return 22
        else:
             return 23
    elif s in stateToID.keys():
        return stateToID[s]
    else:
        return 100 ##error message
#################
CountyInt = 44
latloncounter = 0
M_PATH = "C:\\Users\\Talal\\Documents\\MSE THESIS\\"
path = M_PATH + 'Output\\Module 1\\december\\'
workflowpath = M_PATH + 'Data\\JTW\\workdata.txt'
resflowpath = M_PATH + 'Data\\JTW\\resdata.txt'
workflow = np.loadtxt(workflowpath, dtype = int, delimiter=',',
                      usecols=[0, 1, 5,6, 10])
for countynum, countyRow in enumerate(workflow):
    rs, rc, ws, wc, count = countyRow[0],countyRow[1],countyRow[2],countyRow[3],countyRow[4]
    r = convert_state_county(rs, rc)
    if rs!=34:
        rIndex = r-21
        for i in range(count):
            NonNJdict[rIndex].append(wc)

print "read all of workflow"
for i, region in enumerate(NonNJIDs):
    print "writing " + region
    f = open(path + region + 'Module1NN.csv', 'wb')
    nnjWriter = csv.writer(f, delimiter = ',')
    PERSON_COUNT = 0
    county = CountyInt + i
    latlon = NonNJlatlons[i]
    hht = 9
    tt = 7
    for personWC in NonNJdict[i]:
        income = rd.sample([6,6,6,6,7,7,7,7,7,7,8,8,8,9,9,10], 1)
        incomeAmount = int(rd.uniform(INCOME_BRACKETS[income[0]][0],
                                  INCOME_BRACKETS[income[0]][1]))
        PERSON_COUNT+=1
        idnum = str(100000000 + PERSON_COUNT)
        pid = region + idnum[1:]
        age = rd.randint(22,64)#not of signifcance anymore since tt is fixed
        sex = 1-int(round(rd.uniform(0,.825))) #too biased?
        nnjWriter.writerow([county] + [PERSON_COUNT] + [hht] + [latlon[0]] +
                            [latlon[1]] + [pid] + [age] + [sex] + [tt] +
                            [income] + [incomeAmount])

